package com.example.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@RequiredArgsConstructor
public class SignUpRequestDTO {

    private Long userId;

    @NotBlank
    @Size(min = 3, max = 20)
    private String userName;

    @NotBlank
    private String fullName;

    @NotBlank
    private String age;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(min = 5, max = 40)
    private String password;

    @NotBlank
    private String gender;

    @NotBlank
    private String address;

    private String role;
}
