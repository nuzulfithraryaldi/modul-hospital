package com.example.dto;

import lombok.Data;

@Data
public class SignUpResponseDTO{
    private String fullName;
    private String userName;
}
