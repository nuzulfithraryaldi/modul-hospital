package com.example.controller;

import com.example.Igronerequest;
import com.example.StoreClient;
import com.example.dto.PatientCreateResponseDTO;
import com.example.dto.PatientRequestDTO;
import com.example.service.UserService;
import com.example.dto.SignUpResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/account")
public class AccountController {

    private final StoreClient storeClient;

    private final UserService userService;

    @PostMapping("/addAdmin")
    public SignUpResponseDTO signUpAdmin(@Valid @RequestBody Igronerequest.AddAdmin request){
        return userService.signUpAdmin(request);
    }

//    @PostMapping("/addPatientByAdmin")
//    public PatientCreateResponseDTO create(@Valid @RequestBody PatientRequestDTO request){
//        return storeClient.addPatient(request);
//    }


}
