package com.example.mapper;

import com.example.dto.SignUpRequestDTO;
import com.example.entity.Users;
import com.example.dto.SignUpResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SignUpMapper {

    SignUpMapper INSTANCE = Mappers.getMapper(SignUpMapper.class);

    SignUpResponseDTO toResponse(Users d);

    Users toEntity(SignUpRequestDTO x);
}
