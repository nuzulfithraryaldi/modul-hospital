package com.example.service.implement;


import com.example.entity.UserEnum;
import com.example.dto.SignUpRequestDTO;
import com.example.entity.Users;
import com.example.mapper.SignUpMapper;
import com.example.service.UserService;
import com.example.repository.AdminRepository;
import com.example.dto.SignUpResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final AdminRepository adminRepository;

    @Override
    public SignUpResponseDTO signUpAdmin(SignUpRequestDTO request) {
        try {
            Users entity = SignUpMapper.INSTANCE.toEntity(request);
            request.setRole(UserEnum.USER_TYPE_ADMIN.name());
            if (request.getRole() != null && request.getRole() != "") {
                entity.setRole(request.getRole());
            }
            Users result = adminRepository.save(entity);
            return SignUpMapper.INSTANCE.toResponse(result);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}

