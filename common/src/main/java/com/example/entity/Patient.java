package com.example.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;


@Data
@RequiredArgsConstructor
@Entity
@Table(name="patients_table")
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long patientId;

    private String patientName;

    private String birthPlace;

    private String birthDate;

    private String address;

    private String gender;

    private String complaints;

    private Instant registrationDate;

    @OneToMany(targetEntity = Treatment.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "patientId", referencedColumnName = "patientId")
    private List<Treatment> treatments;

    @ManyToOne
    @JoinColumn(name = "userId")
    private Users users;

}
