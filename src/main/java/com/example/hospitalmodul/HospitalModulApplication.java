package com.example.hospitalmodul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalModulApplication {

	public static void main(String[] args) {
		SpringApplication.run(HospitalModulApplication.class, args);
	}

}
