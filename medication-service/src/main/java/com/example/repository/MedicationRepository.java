package com.example.repository;



import com.example.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long>{
//    @Query(value = "SELECT * FROM medication_table m WHERE m.medicationName LIKE :param%", nativeQuery = true)
    List<Medication> findByMedicationNameLike(String param);
}
