package com.example.dto;

import lombok.Data;

@Data
public class MedicationTreatmentDTO {
    private Long medicationId;
}
