package com.example.dto;
import lombok.Data;

@Data
public class InquiryName {
    private String value;
}
