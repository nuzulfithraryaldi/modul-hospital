package com.example.dto;
import lombok.Data;

@Data
public class MedicationRequestDTO {
    private Long medicationId;

    private String medicationName;

    private String medicationDose;
}
