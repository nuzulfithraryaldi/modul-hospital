package com.example.controller;

import com.example.dto.MedicationRequestDTO;
import com.example.repository.MedicationRepository;
import com.example.service.MedicationService;
import com.example.service.impl.MedicationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/medication")
public class MedicationController {

    @Autowired
    MedicationRepository medicationRepository;

    @Autowired
    MedicationService medicationService;

    @PostMapping("/addMedication")
    public ResponseEntity<?> addMedication(@Valid @RequestBody MedicationRequestDTO request) {
        try {
            medicationRepository.save(MedicationServiceImpl.toMedication(request));
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
