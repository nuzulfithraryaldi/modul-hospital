package com.example.service.impl;

import com.example.entity.Medication;
import com.example.dto.MedicationRequestDTO;
import com.example.service.MedicationService;
import org.springframework.stereotype.Service;

@Service
public class MedicationServiceImpl extends MedicationService {

    public static Medication toMedication(MedicationRequestDTO req) {
        Medication model = new Medication();
        if (req.getMedicationId() != null) {
            model.setMedicationId(req.getMedicationId());
        }

        model.setMedicationName(req.getMedicationName());
        model.setMedicationDose(req.getMedicationDose());
        return model;
    }
}
