package com.example.service;

import com.example.entity.Medication;
import com.example.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MedicationService {
    @Autowired
    MedicationRepository repository;

    public List<Medication> inquiryMedication(String param){
        return repository.findByMedicationNameLike(param);
    }
}
