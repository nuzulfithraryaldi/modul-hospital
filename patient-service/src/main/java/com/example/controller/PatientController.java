package com.example.controller;

import com.example.dto.PatientCreateResponseDTO;
import com.example.dto.PatientRequestDTO;
import com.example.service.PatientService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/patient")
public class PatientController {

    private final PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @PostMapping("/addPatient")
    public PatientCreateResponseDTO create(@Valid @RequestBody PatientRequestDTO request) {
        return patientService.createPatient(request);
    }

    @GetMapping("/getPatientById/{patientId}")
    public PatientCreateResponseDTO getPatientById(@PathVariable Long patientId) {
        return patientService.getPatientById(patientId);
    }
}
