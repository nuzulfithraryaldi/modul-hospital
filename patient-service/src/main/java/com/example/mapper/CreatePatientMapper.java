package com.example.mapper;

import com.example.dto.PatientCreateResponseDTO;
import com.example.dto.PatientRequestDTO;
import com.example.entity.Patient;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CreatePatientMapper {
    CreatePatientMapper INSTANCE = Mappers.getMapper(CreatePatientMapper.class);

    Patient toEntity(PatientRequestDTO v);

    @Mapping(source = "users.userId",target = "userId")
    PatientRequestDTO toDTO(Patient x);

    PatientCreateResponseDTO toResponse(Patient result);
}
