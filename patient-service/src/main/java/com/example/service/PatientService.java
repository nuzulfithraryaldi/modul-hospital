package com.example.service;

import com.example.dto.PatientRequestDTO;
import com.example.dto.PatientCreateResponseDTO;
import org.springframework.http.ResponseEntity;

public interface PatientService {


    PatientCreateResponseDTO createPatient(PatientRequestDTO request);

    ResponseEntity<?> editPatient(PatientRequestDTO patientRequest);

    ResponseEntity<Object> deletePatientById(Long patientId);

    PatientCreateResponseDTO getPatientById (Long patientId);
}
