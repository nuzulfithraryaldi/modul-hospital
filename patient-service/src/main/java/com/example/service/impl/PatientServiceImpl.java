package com.example.service.impl;

import com.example.dto.PatientRequestDTO;
import com.example.entity.Patient;
import com.example.dto.PatientCreateResponseDTO;
import com.example.mapper.CreatePatientMapper;
import com.example.repository.AdminRepository;
import com.example.repository.PatientRepository;
import com.example.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    private final AdminRepository adminRepository;


    @Override
    public PatientCreateResponseDTO createPatient(PatientRequestDTO request) {
        Patient entity = CreatePatientMapper.INSTANCE.toEntity(request);
        entity.setUsers(adminRepository.findById(request.getUserId()).orElse(null));
        Patient result = patientRepository.save(entity);
        return CreatePatientMapper.INSTANCE.toResponse(result);
    }

    @Override
    public ResponseEntity<?> editPatient(PatientRequestDTO patientRequest) {
        return null;
    }

    @Override
    public ResponseEntity<Object> deletePatientById(Long patientId) {
        return null;
    }

    @Override
    public PatientCreateResponseDTO getPatientById(Long patientId) {
        try {
            Patient patient = patientRepository.findById(patientId).orElse(null);
            return CreatePatientMapper.INSTANCE.toResponse(patient);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
