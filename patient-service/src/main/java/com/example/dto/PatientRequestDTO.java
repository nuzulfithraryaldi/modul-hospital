package com.example.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class PatientRequestDTO {
    private Long patientId;

    private Long userId;

    private String patientName;

    private String birthPlace;

    private String birthDate;

    private String address;

    private String gender;

    private String complaints;

    private Instant registrationDate;
}
