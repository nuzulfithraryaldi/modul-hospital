package com.example.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class PatientCreateResponseDTO{
    private String patientName;
    private String complaints;
}
