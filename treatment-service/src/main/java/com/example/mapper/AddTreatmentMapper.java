package com.example.mapper;

import com.example.entity.Treatment;
import com.example.dto.TreatmentRequestDTO;
import com.example.dto.TreatmentResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AddTreatmentMapper {
    AddTreatmentMapper INSTANCE = Mappers.getMapper(AddTreatmentMapper.class);

    Treatment toEntity(TreatmentRequestDTO v);

    @Mapping(source = "patients.patientId",target = "patientId")
    TreatmentRequestDTO toDTO(Treatment x);

    @Mapping(source = "patients.patientId",target = "patientId")
    TreatmentResponseDTO toResponse(Treatment x);
}
