package com.example.dto;

import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class TreatmentRequestDTO {
    private Long treatmentId;

    private Long patientId;

    private String sickness;

    private String sicknessDesc;

    private String sicknessHandling;

    private Instant createTime;

    private List<MedicationTreatmentDTO> medications;
}
