package com.example.dto;

import lombok.Data;

@Data
public class TreatmentResponseDTO {

    private String patientId;
    private String sickness;
}
