package com.example.controller;

import com.example.dto.TreatmentRequestDTO;
import com.example.dto.TreatmentResponseDTO;
import com.example.service.TreatmentService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/treatment")
public class TreatmentController {

    private final TreatmentService treatmentService;

    public TreatmentController(TreatmentService treatmentService) {
        this.treatmentService = treatmentService;
    }

    @PostMapping("/addTreatment")
    public TreatmentResponseDTO addTreatment(@Valid @RequestBody TreatmentRequestDTO request) {
        return treatmentService.addTreatment(request);
    }
}
