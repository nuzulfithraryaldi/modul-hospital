package com.example.service.impl;

import com.example.dto.TreatmentRequestDTO;
import com.example.dto.TreatmentResponseDTO;
import com.example.entity.Treatment;
import com.example.mapper.AddTreatmentMapper;
import com.example.repository.PatientDRepository;
import com.example.repository.TreatmentRepository;
import com.example.service.TreatmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TreatmentServiceImpl implements TreatmentService {

    private final PatientDRepository patRepository;

    private final TreatmentRepository treatmentRepository;

    @Override
    public TreatmentResponseDTO addTreatment(TreatmentRequestDTO treatmentRequest) {
        Treatment entity = AddTreatmentMapper.INSTANCE.toEntity(treatmentRequest);
        entity.setPatients(patRepository.findById(treatmentRequest.getPatientId()).orElse(null));
        Treatment result = treatmentRepository.save(entity);
        return AddTreatmentMapper.INSTANCE.toResponse(result);
    }
}
