package com.example.service;

import com.example.dto.TreatmentRequestDTO;
import com.example.dto.TreatmentResponseDTO;


public interface TreatmentService {

    TreatmentResponseDTO addTreatment(TreatmentRequestDTO treatmentRequest);
}
