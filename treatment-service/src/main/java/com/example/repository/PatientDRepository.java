package com.example.repository;

import com.example.entity.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientDRepository extends JpaRepository<Patient, Long> {
    @Query(value = "SELECT * FROM patients_table p"
            + " JOIN user_table ut ON p.userId  = ut.user_id"
            + " WHERE p.patientName LIKE :#{#param.patientName}%"
            + " AND ut.role = 'USER_TYPE_DOCTOR'"
            + " AND ut.userId = :#{#param.userId}", nativeQuery = true)
//    List<Patient> inquiryPatient(@Param("param") PatientInquiry param);
    Page<Patient> findByPatientNameLikeAndUsers_RoleAndUsers_UserId(String patientName, String role, Long userId, Pageable page);

//    @Query(value = "SELECT * FROM patients_table p"
//            + " WHERE p.patientName LIKE :param%", nativeQuery = true)
    List<Patient> findByPatientNameLike(String param);
}
