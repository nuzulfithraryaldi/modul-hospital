package com.example.mapper;

import com.example.dto.DoctorSignUpRequestDTO;
import com.example.dto.DoctorSignUpResponseDTO;
import com.example.entity.Users;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DoctorSignUpMapper {

    DoctorSignUpMapper INSTANCE = Mappers.getMapper(DoctorSignUpMapper.class);

    DoctorSignUpResponseDTO toResponse(Users d);

    Users toEntity(DoctorSignUpRequestDTO x);
}
