package com.example.dto;

import lombok.Data;

@Data
public class DoctorSignUpResponseDTO {
    private String fullName;
    private String userName;
}
