package com.example.controller;

import com.example.dto.DoctorSignUpRequestDTO;
import com.example.dto.DoctorSignUpResponseDTO;
import com.example.service.DoctorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/account")
public class DoctorController {

    private final DoctorService userService;
    @PostMapping("/addDoctor")
    public DoctorSignUpResponseDTO signUpDoctor(@Valid @RequestBody DoctorSignUpRequestDTO request){
        return userService.signUpDoctor(request);
    }


}
