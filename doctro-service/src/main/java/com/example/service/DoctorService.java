package com.example.service;

import com.example.dto.DoctorSignUpRequestDTO;
import com.example.dto.DoctorSignUpResponseDTO;

public interface DoctorService {

    DoctorSignUpResponseDTO signUpDoctor(DoctorSignUpRequestDTO signUpRequestDTO);
}
