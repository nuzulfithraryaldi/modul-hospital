package com.example.service.impl;

import com.example.dto.DoctorSignUpRequestDTO;
import com.example.dto.DoctorSignUpResponseDTO;
import com.example.entity.UserEnum;
import com.example.entity.Users;
import com.example.mapper.DoctorSignUpMapper;
import com.example.repository.DoctorRepository;
import com.example.service.DoctorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;

    @Override
    public DoctorSignUpResponseDTO signUpDoctor(DoctorSignUpRequestDTO request) {
        try {
            Users entity = DoctorSignUpMapper.INSTANCE.toEntity(request);
            request.setRole(UserEnum.USER_TYPE_DOCTOR.name());
            if (request.getRole() != null && request.getRole() != "") {
                entity.setRole(request.getRole());
            }
            Users result = doctorRepository.save(entity);
            return DoctorSignUpMapper.INSTANCE.toResponse(result);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
