package com.example.service.implement;

import com.example.dto.LoginRequestDTO;
import com.example.dto.LoginResponseDTO;
import com.example.entity.Users;
import com.example.mapper.LoginMapper;
import com.example.repository.UserRepository;
import com.example.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public LoginResponseDTO login(LoginRequestDTO loginRequest){
        try {
            Users user = userRepository.findByUserName(loginRequest.getUserName()).orElse(null);
            LoginResponseDTO responseDTO = LoginMapper.INSTANCE.toDTO(user);
            if (user == null) {
                return responseDTO;
            }
            if (!loginRequest.getPassword().equals(user.getPassword())) {
                return responseDTO;
            }
            return responseDTO;
        } catch (Exception e) {
            return null;
        }
    }
}

