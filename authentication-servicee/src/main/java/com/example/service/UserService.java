package com.example.service;

import com.example.dto.LoginRequestDTO;
import com.example.dto.LoginResponseDTO;

public interface UserService {
    LoginResponseDTO login (LoginRequestDTO loginRequest);
}
